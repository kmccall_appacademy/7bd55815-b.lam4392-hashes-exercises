# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_hash = {}
  str.split.each do |word|
    word_hash[word] = word.length
  end
  word_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |_key, int| int }.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |inventory, value|
    older[inventory] = value
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_frequencies = Hash.new(0)
  word.each_char do |letter|
    letter_frequencies[letter] += 1
  end
  letter_frequencies
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  unique_array = {}
  arr.each do |el|
    unique_array[el] = el
  end
  unique_array.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  evens_odds = Hash.new(0)
  numbers.each do |num|
    if num.even?
      evens_odds[:even] += 1
    else
      evens_odds[:odd] += 1
    end
  end
  evens_odds
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = Hash.new(0)
  string.delete(' ').each_char do |letter|
    vowels[letter] += 1 if 'aeiou'.include?(letter)
  end
  most_vowels = vowels.values.max
  vowels.select { |_letter, freq| freq == most_vowels }.sort.first[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  birthday_combos = []
  fall_winter_birthdays = []
  students.each do |student, month|
    fall_winter_birthdays << student if month > 6
  end

  fall_winter_birthdays.each_with_index do |student, index|
    i = index + 1
    while i < fall_winter_birthdays.length
      student2 = fall_winter_birthdays[i]
      birthday_combos << [student, student2]
      i += 1
    end
  end

  birthday_combos
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  populations = Hash.new(0)
  specimens.each do |specimen|
    populations[specimen] += 1
  end

  num_species = populations.length
  smallest_pop = populations.values.min
  largest_pop = populations.values.max

  (num_species**2) * smallest_pop / largest_pop
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_letters = character_count(normal_sign)
  vandalize_letters = character_count(vandalized_sign)

  vandalize_letters.each do |letter, freq|
    return false if freq > normal_letters[letter]
  end
  true
end

def character_count(str)
  char_count = Hash.new(0)
  str.delete(' ').each_char do |letter|
    char_count[letter.downcase] += 1
  end
  char_count
end
